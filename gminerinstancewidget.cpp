#include "gminerinstancewidget.h"
#include "debuglog.h"
#include <QHBoxLayout>

/*
 * QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
 * env.insert("PATH", env.value("Path") + ";C:\\Bin");
    process.setProcessEnvironment(env);


    QProcess *myProcess = new QProcess(this);
    DebugLog("starting proces '%s'", qPrintable(application_));
    myProcess->start(application_, QStringList());
    myProcess->waitForFinished();
    // canReadLine()  QByteArray	readLine(qint64 maxSize = 0)
*/


GMinerInstanceWidget::GMinerInstanceWidget(QWidget *parent) :
    QWidget(parent),
    wantRunning_(false),
    process_(this),
    title_("GMinerInstance"),
    environment_(QProcessEnvironment::systemEnvironment()),
    state_(GMinerInstanceWidget::sNone),
    pconfig_(0),
    ptxtTitle(new QLineEdit(this)),
    plblStatus(new QLabel(this)),
    pbtnStartStop(new QPushButton(this))
{
    QHBoxLayout* layout = new QHBoxLayout();

    ptxtTitle->setText(title_);
    pbtnStartStop->setText("Start");
    pbtnStartStop->setCheckable(true);
    pbtnStartStop->setChecked(false);


    layout->addWidget(ptxtTitle, Qt::AlignTop | Qt::AlignLeft);
    layout->addWidget(plblStatus, Qt::AlignTop | Qt::AlignLeft);
    layout->addWidget(pbtnStartStop, Qt::AlignTop | Qt::AlignLeft);

    setLayout(layout);

    connect(&process_, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(processFinished(int,QProcess::ExitStatus)));
    connect(&process_, SIGNAL(error(QProcess::ProcessError)), this, SLOT(processError(QProcess::ProcessError)));
    connect(&process_, SIGNAL(started()), this, SLOT(processStarted()));
    connect(&process_, SIGNAL(readyReadStandardOutput()), this, SLOT(processReadyReadStdOut()));
    connect(&process_, SIGNAL(readyReadStandardError()), this, SLOT(processReadyReadStdError()));
    connect(pbtnStartStop, SIGNAL(clicked(bool)), this, SLOT(bntStartStopClicked(bool)));
}

QProcess& GMinerInstanceWidget::process()
{
    return process_;
}

const QProcess& GMinerInstanceWidget::process() const
{
    return process_;
}

void GMinerInstanceWidget::processError(QProcess::ProcessError error)
{
    const char* pMsg;
    switch (error)
    {
        case QProcess::FailedToStart:   pMsg = "FailedToStart"; break;
        case QProcess::Crashed:         pMsg = "Crashed";       break;
        case QProcess::Timedout:        pMsg = "Timedout";      break;
        case QProcess::WriteError:      pMsg = "WriteError";    break;
        case QProcess::ReadError:       pMsg = "ReadError";     break;
        case QProcess::UnknownError:    pMsg = "UnknownError";  break;
        default:                        pMsg = "(default)";     break;

    }

    DebugLog("Process Error %d %s", (int)error, pMsg);
    setState(GMinerInstanceWidget::sError);
}

void GMinerInstanceWidget::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    DebugLog("GMinerInstanceWidget::processFinished");
    //QProcess::NormalExit
    DebugLog("Process Finished  want running %c exit code %d, exit status %d", BOOL_YN(wantRunning_), exitCode, (int)exitStatus);
    setState(GMinerInstanceWidget::sStopped);
}

void GMinerInstanceWidget::processStarted()
{
    DebugLog("GMinerInstanceWidget::processStarted");
    setState(GMinerInstanceWidget::sStarted);
}

void GMinerInstanceWidget::setTitle(const QString& title)
{
    title_ = title;
    ptxtTitle->setText(title);
}

const QString& GMinerInstanceWidget::title() const
{
    return title_;
}

void GMinerInstanceWidget::setFromConfig(const GMinerConfig& config)
{
    setApplication(config.application());

    environment_.clear();
    QMapIterator<QString, QString> i(config.enironmentVars());
    while (i.hasNext())
    {
        i.next();
        DebugLog("setFromConfig env '%s'->'%s'", qPrintable(i.key()), qPrintable(i.value()));
        environment_.insert(i.key(), i.value()); // QProcessEnvironment
    }
    arguments_.clear();
    arguments_.append(config.arguments());
}

void GMinerInstanceWidget::setApplication(const QString& app)
{
    application_ = app;
}

const QString& GMinerInstanceWidget::application() const
{
    return application_;
}

QStringList& GMinerInstanceWidget::arguments()
{
    return arguments_;
}

const QStringList& GMinerInstanceWidget::arguments() const
{
    return arguments_;
}

QProcessEnvironment& GMinerInstanceWidget::environment()
{
    return environment_;
}

const QProcessEnvironment& GMinerInstanceWidget::environment() const
{
    return environment_;
}

GMinerInstanceWidget::enumState GMinerInstanceWidget::state() const
{
    return state_;
}

void GMinerInstanceWidget::setState(GMinerInstanceWidget::enumState newState)
{
    state_ = newState;
    emit stateChanged(state_);
}

void GMinerInstanceWidget::start()
{
    QString configFileName = "./aguGMiner.conf";
    QStringList fullArguments;
    DebugLog("GMinerInstanceWidget::start title: '%s' app: '%s'", qPrintable(title_), qPrintable(application_));

    wantRunning_ = true;

    fullArguments = arguments_;

    if (pconfig_)
    {
        fullArguments.append(QString("-c %1").arg(configFileName));
        if (!pconfig_->writeConfigFile(configFileName))
        {
            DebugLog("GMinerInstanceWidget::start writeConfigFile failed filename: '%s'", qPrintable(configFileName));
        }
    }
    else DebugLog("GMinerInstanceWidget::start no config pointer set");

    process_.setProcessEnvironment(environment_);

    setState(GMinerInstanceWidget::sStarting);
    process_.start(application_, arguments_);
    pbtnStartStop->setText("Stop");
    pbtnStartStop->setChecked(true);

}

void GMinerInstanceWidget::stop()
{
    DebugLog("GMinerInstanceWidget::stop title: '%s'", qPrintable(title_));
    wantRunning_ = false;

    setState(GMinerInstanceWidget::sStopping);

    process_.kill();
    pbtnStartStop->setText("Start");
    pbtnStartStop->setChecked(false);
}

void GMinerInstanceWidget::bntStartStopClicked(bool isChecked)
{
    if (isChecked)
        start();
    else
        stop();
}

void GMinerInstanceWidget::setConfig(GMinerConfig* pconfig)
{
    pconfig_ = pconfig;
    setFromConfig(*pconfig);
}

GMinerConfig* GMinerInstanceWidget::config()
{
    return pconfig_;
}

const GMinerConfig* GMinerInstanceWidget::config() const
{
    return pconfig_;
}

QStringList& GMinerInstanceWidget::outputText()
{
    return outputText_;
}

const QStringList& GMinerInstanceWidget::outputText() const
{
    return outputText_;
}

void GMinerInstanceWidget::processReadyReadStdOut()
{
    outputText_.append(process_.readAllStandardOutput().constData());
    emit outputTextChanged(this);
}

void GMinerInstanceWidget::processReadyReadStdError()
{
    outputText_.append(process_.readAllStandardError().constData());
    emit outputTextChanged(this);

}
