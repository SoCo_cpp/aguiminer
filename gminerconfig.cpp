#include "gminerconfig.h"
#include "debuglog.h"
#include <QFile>

GMinerConfig::GMinerConfig(QObject *parent) :
    QObject(parent),
    application_("sgminer"),
    threadConcurrency_(0),
    intensity_(13),
    threads_(0),
    workSize_(0),
    useEngineClock_(false),
    engineClock_(0),
    useMemoryClock_(false),
    memoryClock_(0),
    autoFan_(true)
{
    //--------------------------
    // Temporary default environment variables
    mapEnvironmentVars_.insert("DISPLAY", ":0");
    mapEnvironmentVars_.insert("GPU_MAX_ALLOC_PERCENT", "100");
    mapEnvironmentVars_.insert("GPU_USE_SYNC_OBJECTS", "1");
    //--------------------------
}

const QString& GMinerConfig::application() const
{
    return application_;
}

void GMinerConfig::setApplication(const QString& app)
{
    application_ = app;
}

unsigned int GMinerConfig::threadConcurrency() const
{
    return threadConcurrency_;
}

void GMinerConfig::setThreadConcurrency(int tc)
{
    threadConcurrency_ = tc;
}

unsigned int GMinerConfig::intensity() const
{
    return intensity_;
}

void GMinerConfig::setIntensity(int i)
{
    intensity_ = i;
}

unsigned int GMinerConfig::threads() const
{
    return threads_;
}

void GMinerConfig::setThreads(int th)
{
    threads_ = th;
}

unsigned int GMinerConfig::workSize() const
{
    return workSize_;
}

void GMinerConfig::setWorkSize(int ws)
{
    workSize_ = ws;
}

unsigned int GMinerConfig::engineClock() const
{
    return engineClock_;
}

bool GMinerConfig::useEngineClock() const
{
    return useEngineClock_;
}

void GMinerConfig::setEngineClock(int clock, bool disabled /*= false*/)
{
    engineClock_ = clock;
    useEngineClock_ = (!disabled);
}

unsigned int GMinerConfig::memoryClock() const
{
    return memoryClock_;
}

bool GMinerConfig::useMemoryClock() const
{
    return useMemoryClock_;
}

void GMinerConfig::setMemoryClock(int clock, bool disabled /*= false*/)
{
    memoryClock_ = clock;
    useMemoryClock_ = (!disabled);
}

bool GMinerConfig::autoFan() const
{
    return autoFan_;
}

void GMinerConfig::setAutoFan(bool fan)
{
    autoFan_ = fan;
}

QList<MiningPool*>& GMinerConfig::miningPools()
{
    return miningPools_;
}

const QList<MiningPool*>& GMinerConfig::miningPools() const
{
    return miningPools_;
}

QMap<QString,QString>& GMinerConfig::enironmentVars()
{
    return mapEnvironmentVars_;
}

const QMap<QString,QString>& GMinerConfig::enironmentVars() const
{
    return mapEnvironmentVars_;
}

QStringList& GMinerConfig::arguments()
{
    return arguments_;
}

const QStringList& GMinerConfig::arguments() const
{
    return arguments_;
}

QString GMinerConfig::buildCoinfig() const
{
    //@TODO cleanup using QStringList
    QString s;

    s = "{\n"; // first open
    s += buildConfigPools();
    s += "\n"; // extra line after pools

    s += QString("\"thread-concurrency\" : \"%1\",\n").arg(threadConcurrency_);
    s += QString("\"intensity\" : \"%1\",\n").arg(intensity_);
    s += QString("\"gpu-threads\" : \"%1\",\n").arg(threads_);
    s += QString("\"worksize\" : \"%1\",\n").arg(workSize_);

    if (useEngineClock_)
        s += QString("\"gpu-engine\" : \"%1-%1\",\n").arg(engineClock_);

    if (useMemoryClock_)
        s += QString("\"gpu-memclock\" : \"%1\",\n").arg(memoryClock_);

    s += QString("\"auto-fan\" : %1\n").arg( (autoFan_ ? "true" : "false") ); // no quotes for bools, Last group, no comma

    s += "}\n"; // final closing

    return s;
}

QString GMinerConfig::buildConfigPools() const
{
    QString s;
    s = "\"pools\" : [\n";
    for (int i = 0;i < miningPools_.size();i++)
    {
        if (i)
            s += ",\n"; // finish last pool group
        s += miningPools_[i]->buildConfig();
    }
    s += "\n"; // finish last pool group
    s += "],\n";
    return s;
}


bool GMinerConfig::writeConfigFile(QString fileName) const
{
    QFile f(fileName);
    if (!f.open(QIODevice::Text | QIODevice::WriteOnly | QIODevice::Truncate))
    {
        DebugLog("writeConfigFile open file for writing failed");
        return false;
    }
    if (f.write(buildCoinfig().toLatin1()) == -1)
    {
        DebugLog("writeConfig write failed");
        return false;
    }
    return true;
}


bool GMinerConfig::startProcess()
{
    return true;
}
