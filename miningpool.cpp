#include "miningpool.h"

MiningPool::MiningPool(QObject *parent) :
    QObject(parent)
{
}

const QString& MiningPool::title() const
{
    return title_;
}

void MiningPool::setTitle(const QString& ttitle)
{
    title_ = ttitle;
}

const QString& MiningPool::coin() const
{
    return coin_;
}

void MiningPool::setCoin(const QString& tcoin)
{
    coin_ = tcoin;
}

const QString& MiningPool::address() const
{
    return address_;
}

void MiningPool::setAddress(const QString& taddress)
{
    address_ = taddress;
}

const QString& MiningPool::user() const
{
    return user_;
}

void MiningPool::setUser(const QString& tuser)
{
    user_ = tuser;
}

const QString& MiningPool::password() const
{
    return password_;
}

void MiningPool::setPassword(const QString& tpassword)
{
    password_ = tpassword;
}

QString MiningPool::buildConfig() const
{
    QString s;

    s = "\t{\n\t\t\"url\" : \"";
    s += address_;
    s += "\",\n\t\t\"user\" : \"";
    s += user_;
    s += "\",\n\t\t\"pass\" : \"";
    s += password_;
    s += "\"\n\t}"; // no new line

    return s;
}
