#-------------------------------------------------
#
# Project created by QtCreator 2014-01-23T17:36:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AGUIMiner
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sgminerwidget.cpp \
    gminerconfig.cpp \
    miningpool.cpp \
    debuglog.cpp \
    gminerinstancewidget.cpp

HEADERS  += mainwindow.h \
    sgminerwidget.h \
    gminerconfig.h \
    miningpool.h \
    debuglog.h \
    gminerinstancewidget.h

FORMS    += mainwindow.ui \
    sgminerwidget.ui
