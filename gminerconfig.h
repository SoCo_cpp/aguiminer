#ifndef GMINERCONFIG_H
#define GMINERCONFIG_H

#include "miningpool.h"
#include <QObject>
#include <QString>
#include <QList>
#include <QMap>
#include <QStringList>

class GMinerConfig : public QObject
{
    Q_OBJECT
public:
    explicit GMinerConfig(QObject *parent = 0);

    const QString& application() const;
    void setApplication(const QString& app);

    unsigned int threadConcurrency() const;
    void setThreadConcurrency(int tc);

    unsigned int intensity() const;
    void setIntensity(int i);

    unsigned int threads() const;
    void setThreads(int th);

    unsigned int workSize() const;
    void setWorkSize(int ws);

    unsigned int engineClock() const;
    bool useEngineClock() const;
    void setEngineClock(int clock, bool disabled = false);

    unsigned int memoryClock() const;
    bool useMemoryClock() const;
    void setMemoryClock(int clock, bool disabled = false);

    bool autoFan() const;
    void setAutoFan(bool fan);

    QList<MiningPool*>& miningPools();
    const QList<MiningPool*>& miningPools() const;

    QMap<QString,QString>& enironmentVars();
    const QMap<QString,QString>& enironmentVars() const;

    QStringList& arguments();
    const QStringList& arguments() const;

    QString buildCoinfig() const;
    QString buildConfigPools() const;

    bool writeConfigFile(QString fileName) const;
    bool startProcess();


private:
    QString application_;
    unsigned int threadConcurrency_;
    unsigned int intensity_;
    unsigned int threads_;
    unsigned int workSize_;
    bool useEngineClock_;
    unsigned int engineClock_;
    bool useMemoryClock_;
    unsigned int memoryClock_;
    bool autoFan_;
    QList<MiningPool*> miningPools_;
    QMap<QString,QString> mapEnvironmentVars_;
    QStringList arguments_;


signals:

public slots:

};

#endif // GMINERCONFIG_H
