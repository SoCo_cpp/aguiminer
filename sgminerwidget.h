#ifndef SGMINERWIDGET_H
#define SGMINERWIDGET_H

#include "gminerconfig.h"
#include "gminerinstancewidget.h"

#include <QWidget>
#include <QTimer>
#include <QTextStream>
#include <QVBoxLayout>

#define C_POOL_COL_COUNT     4
#define C_POOL_COL_TITLE     0
#define C_POOL_COL_ADDRESS   1
#define C_POOL_COL_USER      2
#define C_POOL_COL_PASSWORD  3

namespace Ui {
class SGMinerWidget;
}

class SGMinerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SGMinerWidget(QWidget *parent = 0);
    ~SGMinerWidget();

    GMinerConfig& config();
    const GMinerConfig& config() const;

    void fillConfig();
    void readConfig();
    void updateEnabled();

    void fillPools();
    void fillInstances();

    void configChanged();

    QList<GMinerInstanceWidget*>& instances();
    const QList<GMinerInstanceWidget*>& instances() const;

private:
    Ui::SGMinerWidget *ui;
    QVBoxLayout* layoutInstances_;
    GMinerConfig config_;
    bool updatingConfig_;
    bool updatingPools_;
    QTimer configChangedTimer_;
    QList<GMinerInstanceWidget*> instances_;


private slots:
    void on_configUIChangedString(const QString&);
    void on_configUIChangedCeckbox(int);
    void on_configUIChanged();

    void on_btnAddPool_clicked();
    void on_btnDeletePool_clicked();

    void on_btnPoolUp_clicked();
    void on_btnPoolDown_clicked();

    void on_tablePoolsCellChanged(int row, int col);

    void on_btnSaveScript_clicked();
    void onInstanceOutputChanged(GMinerInstanceWidget* inst);

signals:


};

#endif // SGMINERWIDGET_H
