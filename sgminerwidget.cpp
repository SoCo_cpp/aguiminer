#include "sgminerwidget.h"
#include "ui_sgminerwidget.h"
#include "miningpool.h"
#include "debuglog.h"
#include <QVBoxLayout>
#include <QMapIterator>

SGMinerWidget::SGMinerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SGMinerWidget),
    layoutInstances_(new QVBoxLayout(this)),
    config_(this),
    updatingConfig_(false),
    updatingPools_(false)
{
    ui->setupUi(this);
    ui->tablePools->setColumnCount(C_POOL_COL_COUNT);

    ui->wInstanceContainer->setLayout(layoutInstances_);

    fillConfig();
    fillInstances();

    connect(ui->txtMinerApp,        SIGNAL(textChanged(const QString &)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinConcurrency,    SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinIntensity,	    SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinGPUThreads,	    SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinWorkSize,	    SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinEngine,         SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinMemory,         SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinConcurrency,    SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));
    connect(ui->spinConcurrency,    SIGNAL(valueChanged(const QString&)), this, SLOT(on_configUIChangedString(const QString&)));

    connect(ui->cbEngineClock,      SIGNAL(stateChanged(int)), this, SLOT(on_configUIChangedCeckbox(int)));
    connect(ui->cbMemoryClock,      SIGNAL(stateChanged(int)), this, SLOT(on_configUIChangedCeckbox(int)));
    connect(ui->cbAutoFan,          SIGNAL(stateChanged(int)), this, SLOT(on_configUIChangedCeckbox(int)));
    connect(ui->tablePools,         SIGNAL(cellChanged(int,int)), this, SLOT(on_tablePoolsCellChanged(int,int)));
}

SGMinerWidget::~SGMinerWidget()
{
    delete ui;
}

GMinerConfig& SGMinerWidget::config()
{
    return config_;
}

const GMinerConfig& SGMinerWidget::config() const
{
    return config_;
}

void SGMinerWidget::fillConfig()
{
    updatingConfig_ = true;
    ui->txtMinerApp->setText(config_.application());
    ui->spinConcurrency->setValue(config_.threadConcurrency());
    ui->spinIntensity->setValue(config_.intensity());
    ui->spinGPUThreads->setValue(config_.threads());
    ui->spinWorkSize->setValue(config_.workSize());
    ui->spinEngine->setValue(config_.engineClock());
    ui->spinMemory->setValue(config_.memoryClock());
    ui->cbAutoFan->setChecked(config_.autoFan());

    ui->cbEngineClock->setChecked(config_.useEngineClock());
    ui->spinEngine->setEnabled(config_.useEngineClock());

    ui->cbMemoryClock->setChecked(config_.useMemoryClock());
    ui->spinMemory->setEnabled(config_.useMemoryClock());
    updatingConfig_ = false;
    updateEnabled();
}

void SGMinerWidget::readConfig()
{
    config_.setApplication(ui->txtMinerApp->text());
    config_.setThreadConcurrency(ui->spinConcurrency->value());
    config_.setIntensity(ui->spinIntensity->value());
    config_.setThreads(ui->spinGPUThreads->value());
    config_.setWorkSize(ui->spinWorkSize->value());
    config_.setEngineClock(ui->spinEngine->value(), !ui->cbEngineClock->isChecked());
    config_.setMemoryClock(ui->spinMemory->value(), !ui->cbMemoryClock->isChecked());
    config_.setAutoFan(ui->cbAutoFan->isChecked());
}

void SGMinerWidget::updateEnabled()
{
    ui->cbEngineClock->setChecked(config_.useEngineClock());
    ui->spinEngine->setEnabled(config_.useEngineClock());

    ui->cbMemoryClock->setChecked(config_.useMemoryClock());
    ui->spinMemory->setEnabled(config_.useMemoryClock());
}

void SGMinerWidget::fillPools()
{
    updatingPools_ = true;
    QList<MiningPool*>& pools(config_.miningPools());

    ui->tablePools->setRowCount(pools.size());
    for (int i = 0;i < pools.size();i++)
    {
        DebugLog("fillPool [%d] t'%s' add'%s' u'%s' p''", i, qPrintable(pools[i]->title()), qPrintable(pools[i]->address()), qPrintable(pools[i]->user()), qPrintable(pools[i]->password()));
        ui->tablePools->setItem(i, C_POOL_COL_TITLE,    new QTableWidgetItem(pools[i]->title()));
        ui->tablePools->setItem(i, C_POOL_COL_ADDRESS,  new QTableWidgetItem(pools[i]->address()));
        ui->tablePools->setItem(i, C_POOL_COL_USER,     new QTableWidgetItem(pools[i]->user()));
        ui->tablePools->setItem(i, C_POOL_COL_PASSWORD, new QTableWidgetItem(pools[i]->password()));
    }
    updatingPools_ = false;
}

void SGMinerWidget::fillInstances()
{
    while (!layoutInstances_->isEmpty())
        layoutInstances_->removeItem(ui->wInstanceContainer->layout()->itemAt(0));

    for (int i = 0;i < instances_.size();i++)
    {
        DebugLog("fill instance [%d]", i);

        layoutInstances_->addWidget(static_cast<QWidget*>(instances_[i]),  Qt::AlignLeft | Qt::AlignTop);
    }
}

void SGMinerWidget::configChanged()
{
    if (updatingConfig_)
        return; // ignore changes while filling controls
    configChangedTimer_.stop();
    configChangedTimer_.singleShot(1000, this, SLOT(on_configUIChanged()));
}

void SGMinerWidget::on_configUIChangedString(const QString&)
{
    configChanged();
}

void SGMinerWidget::on_configUIChangedCeckbox(int)
{
    configChanged();
}

void SGMinerWidget::on_configUIChanged()
{
    readConfig();
    updateEnabled();
}


void SGMinerWidget::on_btnAddPool_clicked()
{
    MiningPool* pool = new MiningPool(this);
    pool->setTitle(QString("New Pool %1").arg(config_.miningPools().size()));
    pool->setAddress("Enter Address");
    pool->setUser("0");
    pool->setPassword("x");
    config_.miningPools().append(pool);
    fillPools();
}

void SGMinerWidget::on_btnDeletePool_clicked()
{
    if (ui->tablePools->currentRow() < config_.miningPools().size())
        config_.miningPools().removeAt(ui->tablePools->currentRow());
    fillPools();
}

void SGMinerWidget::on_btnPoolUp_clicked()
{
    int cur = ui->tablePools->currentRow();
    int prev = cur - 1;
    if (prev >= 0 && cur < config_.miningPools().size())
    {
        DebugLog("Swap up (%d, %d)", cur, prev);
        config_.miningPools().swap(cur, prev);
        fillPools();
        ui->tablePools->setCurrentCell(prev, 0);
    }
}

void SGMinerWidget::on_btnPoolDown_clicked()
{
    int cur = ui->tablePools->currentRow();
    int next = cur + 1;
    if (cur >= 0 && next < config_.miningPools().size())
    {
        DebugLog("Swap down (%d, %d)", cur, next);
        config_.miningPools().swap(cur, next);
        fillPools();
        ui->tablePools->setCurrentCell(next, 0);
    }
}

void SGMinerWidget::on_tablePoolsCellChanged(int row, int col)
{
    if (updatingPools_)
    {
         DebugLog("Pool [%d,%d] updating", row, col);
        return;
    }
    if (row >= 0 && row < config_.miningPools().size())
    {
        MiningPool* pool = config_.miningPools()[row];
        const QString& newvalue = ui->tablePools->item(row, col)->text();
        DebugLog("Pool [%d,%d] newvalue '%s'", row, col, qPrintable(newvalue));
        switch (col)
        {
            case C_POOL_COL_TITLE:      pool->setTitle(newvalue);       break;
            case C_POOL_COL_ADDRESS:    pool->setAddress(newvalue);     break;
            case C_POOL_COL_USER:       pool->setUser(newvalue);        break;
            case C_POOL_COL_PASSWORD:   pool->setPassword(newvalue);    break;
        }
    }
    else DebugLog("Pool cell change: invalid row %d (max %d)", row, config_.miningPools().size());

}

void SGMinerWidget::on_btnSaveScript_clicked()
{
    GMinerInstanceWidget* inst = new GMinerInstanceWidget(this);
    inst->setTitle("sgminer instance");
    inst->setConfig(&config_);

    connect(inst, SIGNAL(outputTextChanged(GMinerInstanceWidget*)), this, SLOT(onInstanceOutputChanged(GMinerInstanceWidget*)));
    instances_.append(inst);

    fillInstances();
    /*
    if (!config_.writeConfigFile())
    {
        DebugLog("write config failed");
        return;
    }
    if (!config_.startProcess())
    {
        DebugLog("start process failed");
        return;
    }
    */
    //DebugLog(" Build test:\r\n---------------------------------------\r\n%s", qPrintable(config_.buildCoinfig()));
}


void SGMinerWidget::onInstanceOutputChanged(GMinerInstanceWidget* inst)
{
    QString log;

    for (int i = 0;i < inst->outputText().size();i++)
        log += inst->outputText()[i];
    ui->txtOutput->document()->setPlainText(log);

}

QList<GMinerInstanceWidget*>& SGMinerWidget::instances()
{
    return instances_;
}

const QList<GMinerInstanceWidget*>& SGMinerWidget::instances() const
{
    return instances_;
}

