#ifndef GMINERINSTANCEWIDGET_H
#define GMINERINSTANCEWIDGET_H

#include <QWidget>
#include <QProcess>
#include <QProcessEnvironment>
#include <QStringList>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include "gminerconfig.h"

class GMinerInstanceWidget : public QWidget
{
    Q_OBJECT
public:
    typedef enum {sNone = 0, sError, sStopping, sStopped, sPaused, sStarting, sStarted } enumState;

    explicit GMinerInstanceWidget(QWidget *parent = 0);

    QProcess& process();
    const QProcess& process() const;

    void setTitle(const QString& title);
    const QString& title() const;

    void setFromConfig(const GMinerConfig& config);

    void setApplication(const QString& app);
    const QString& application() const;

    QStringList& arguments();
    const QStringList& arguments() const;

    QProcessEnvironment& environment();
    const QProcessEnvironment& environment() const;

    GMinerInstanceWidget::enumState state() const;
    void setState(GMinerInstanceWidget::enumState newState); // emits changed

    void setConfig(GMinerConfig* pconfig);
    GMinerConfig* config();
    const GMinerConfig* config() const;

    QStringList& outputText();
    const QStringList& outputText() const;


    void start();
    void stop();

private:
    bool wantRunning_;
    QProcess process_;
    QString title_;
    QString application_;
    QStringList arguments_;
    QProcessEnvironment environment_;
    enumState state_;
    GMinerConfig* pconfig_;
    QStringList outputText_;

    QLineEdit* ptxtTitle;
    QLabel* plblStatus;
    QPushButton* pbtnStartStop;




signals:
    void stateChanged(GMinerInstanceWidget::enumState newState);
    void outputTextChanged(GMinerInstanceWidget* inst);

public slots:

    void processError(QProcess::ProcessError error);
    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void processReadyReadStdOut();
    void processReadyReadStdError();
    void processStarted();
    void bntStartStopClicked(bool isChecked);
};

#endif // GMINERINSTANCEWIDGET_H
