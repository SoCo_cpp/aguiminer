#ifndef MININGPOOL_H
#define MININGPOOL_H

#include <QObject>

class MiningPool : public QObject
{
    Q_OBJECT
public:
    explicit MiningPool(QObject *parent = 0);

    const QString& title() const;
    void setTitle(const QString& ttitle);
    const QString& coin() const;
    void setCoin(const QString& tcoin);
    const QString& address() const;
    void setAddress(const QString& taddress);
    const QString& user() const;
    void setUser(const QString& tuser);
    const QString& password() const;
    void setPassword(const QString& tpassword);

    QString buildConfig() const;
private:
    QString title_;
    QString coin_;
    QString address_;
    QString user_;
    QString password_;

signals:

public slots:

};

#endif // MININGPOOL_H
