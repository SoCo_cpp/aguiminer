#ifndef DEBUGLOG_H
#define DEBUGLOG_H

class QString;
void fDebugLog(const char* fmt, ...);

#define DebugLog fDebugLog
//#define mDebug(fmt,...) (fDebugLog(key,fmt, ##__VA_ARGS__))
#define BOOL_YN(b) ( b ? 'Y' : 'N' )
#define BOOL_TF(b) ( b ? 'T' : 'F' )
#endif // DEBUGLOG_H
