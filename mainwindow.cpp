#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sgminerwidget.h"
#include <QVBoxLayout>
#include <QWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QVBoxLayout* lMain = new QVBoxLayout();

    QVBoxLayout* lControl = new QVBoxLayout();
    QWidget* wControl = new QWidget();
    SGMinerWidget* wSgminer = new SGMinerWidget();
    lControl->addWidget(wSgminer, Qt::AlignLeft | Qt::AlignTop);
    wControl->setLayout(lControl);
    lMain->addWidget(wControl, Qt::AlignLeft | Qt::AlignTop);
    ui->centralWidget->setLayout(lMain);
}

MainWindow::~MainWindow()
{
    delete ui;
}
