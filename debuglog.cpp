#include "debuglog.h"
#include <QString>
#include <QDebug>

void fDebugLog(const char* fmt, ...)
{
    va_list args;
    QString s;
    va_start(args, fmt);
    qDebug() << qPrintable(s.vsprintf(fmt, args));
    va_end(args);
}
